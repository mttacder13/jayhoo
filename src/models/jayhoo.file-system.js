jayhoo.WinFileSystem = function() {
    this.lastError = null;
    this._fso = new ActiveXObject('Scripting.FileSystemObject');
    this._shell = new ActiveXObject('WScript.Shell');
}
jayhoo.WinFileSystem.prototype = {
    /**
     * Reads an INI file. Take note that this is a very simple
     * implementation and does not support INI groups
     * @param  {String} path The path of the INI file
     * @return {Object}      The INI hash object that contains key-value pairs based on the INI file
     */
    readIniFile: function (path) {
        var ini = {},
            stream,
            line,
            equalIndex,
            name,
            value;

        if (this._fso.fileExists(path)) {
            // open the file as text stream
            stream = this._fso.openTextFile(path, 1);

            while (!stream.atEndOfStream) {
                line = stream.readLine();
                // read line-by-line and check if the line is NOT a comment
                // and it should contain the equal character
                if ((equalIndex = line.indexOf('=')) !== -1
                    && line.indexOf('#') !== 0) {
                    name = line.substr(0, equalIndex).trim();
                    value = line.substr(equalIndex + 1).trim();
                    ini[name] = value;
                }
            }
        }
        return ini;
    },
    /**
     * Reads the content of a text file
     * @param  {String} path The path of the text file
     * @return {String}      The String content of the text file
     */
    readTextFile: function(path) {
        var text = null;
        if (this._fso.fileExists(path)) {
            // open the file as a stream and read all
            var file = this._fso.openTextFile(path, 1);
            text = file.readAll();
            file.close();
        }
        return text;
    },
    /**
     * Write the text content to a text file
     * @param  {String} path   The path of the text file
     * @param  {String} text   The content to be written to the text file
     * @param  {Boolean} append A flag that used to indicate if
     *                          we append the content in case the file already exists
     */
    writeTextFile: function(path, text, append) {
        // if the file exists and we are not going to append to it,
        // delete it first
        if (this._fso.fileExists(path) && !append) {
            this._fso.deleteFile(path);
        }
        try {
            // open the text file for appending (8)
            var file = this._fso.openTextFile(path, 8, true);
            file.write(text);
            file.close();
        } catch (err) {
            this.lastError = err;
        }
    },
    /**
     * Delete a file
     * @param  {String} path The path of the file
     * @return {Boolean}      Returns true if successful
     */
    deleteFile: function(path) {
        try {
            this._fso.deleteFile(path);
            return true;
        } catch (err) {
            this.lastError = err;
        }
        return false;
    },
    /**
     * Scans a folder for files and calls out the callback
     * passing a file information object containing:
     *     path      (String) the full path of the file
     *     name      (String) the filename of the file (without extension)
     *     extension (String) a lower-cased file extension, without the dot
     *     size      (Number) file size
     *     type      (String) the type of the file
     *     folder    (String) the folder name containing the file
     *     created   (Date)   the date the file was created
     */
    scanFolder: function(path, callback) {
        // no use to continue if the path does not exist
        if (!this._fso.folderExists(path)) {
            return;
        }
        // create a folder object to get its subfolders
        var folder = this._fso.getFolder(path);
        // create an enumerator object for the folder's subfolders
        var iterator = new Enumerator(folder.subFolders);

        while (!iterator.atEnd()) {
            // recursively call this method again for all subfolders
            this.scanFolder(iterator.item().path, callback);
            iterator.moveNext();
        }

        // create an enumerator for the folder's files
        iterator = new Enumerator(folder.files);

        while (!iterator.atEnd()) {
            var file = iterator.item();

            // sometimes the file is moved to another location
            // and then it's dateCreated attribute is updated
            // so we don't want that date and closest date that
            // we can get is dateAccessed, whichever is the least date
            // will be the date that we will use as date created
            var created = new Date(file.dateCreated);

            var tempDate = file.dateAccessed ? new Date(file.dateAccessed) : created;
            if (tempDate < created) {
                created = tempDate;
            }

            tempDate = new Date(file.dateModified);
            if (tempDate < created) {
                created = tempDate;
            }

            // call the callback function and check to see if we need
            // to iterate all other files
            var result = callback({
                path: file.path,
                // name does not contain the extension
                name: file.name.substr(0, file.name.lastIndexOf('.')),
                // extension does not contain the dot character
                extension: file.name.substr(file.name.lastIndexOf('.') + 1).toLowerCase(),
                size: file.size,
                type: file.type,
                folder: folder.name,
                folderPath: folder.path,
                created: created
            });
            if (result) {
                break;
            }
            iterator.moveNext();
        }
    },
    /**
     * Ensures that the given folder exists
     * @param  {String} path the path of the folder
     * @return {Boolean}      Returns true for success
     */
    ensureFolder: function(path) {
        if (!this._fso.folderExists(path)) {
            try {
                this._fso.createFolder(path);
                return true;
            } catch (err) {
                this.lastError = err;
            }
        }
        return false;
    },
    fileExists: function(path) {
        return this._fso.fileExists(path);
    },
    /**
     * Executes a command
     * @param  {String} command The command String
     */
    exec: function(command) {
        this._shell.run('"' + command + '"', 1);
    },
    /**
     * Opens the folder location using windows explorer
     * @param  {String} path The path of the folder
     */
    openFolder: function(path) {
        this._shell.run('explorer.exe /select, "' + path + '"', 1);
    }
}
