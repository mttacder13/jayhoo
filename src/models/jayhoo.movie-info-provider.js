jayhoo.OmdbMovieMetaProvider = function() {
    // notify subscribers when we are about to download movie information
    this.beforeGetMovieMetaEvent = new jayhoo.Event(this);
    // notify subscribers after we have downloaded movie information
    this.afterGetMovieMetaEvent = new jayhoo.Event(this);
};

jayhoo.OmdbMovieMetaProvider.prototype = {

    /**
     * Checks whether the string is an external id
     * @param  {String}  id the string to test
     * @return {Boolean}
     */
    isValidExternalId: function(id) {
        return id.indexOf('tt') === 0;
    },

    /**
     * Update a movie object's info property
     * @param  {Object} movie   The movie object
     * @param  {Object} options A hash of key-value pairs (title/year or external id)
     */
    updateMovieInfo: function(movie, options) {

        // get the valid params out from options
        var params = false,
            self = this;

        // if an external ID is provided, use that immediately
        if (options.externalId) {
            params = { i: options.externalId };
        } else if (options.title && options.year) {
            params = { t: options.title, y: options.year };
        }

        // if params is not set, then we notify our subscribers and return
        if (!params) {
            this.afterGetMovieMetaEvent.notify({
                success: false,
                movie: movie,
                error: 'Invalid options provided to updateMovieInfo().',
                options: options
            });
            return;
        }

        // notify subscribers that we are about to download movie information
        this.beforeGetMovieMetaEvent.notify({
            movie: movie,
            params: params,
            options: options
        });

        // download the data uisng jsonp
        jayhoo.jsonp.get('http://www.omdbapi.com', params, function(data) {
            if (data && data.Response === 'True') {
                movie.info = {
                    title: data.Title,
                    year:  parseInt(data.Year),
                    rated: data.Rated,
                    released: new Date(data.Released),
                    runtime: data.Runtime,
                    genres: data.Genre,
                    directors: data.Director,
                    writers: data.Writer,
                    actors: data.Actors,
                    plot: data.Plot,
                    poster: isValidUrl(data.Poster) ? data.Poster : '',
                    rating: parseFloat(data.imdbRating),
                    votes: data.imdbVotes,
                    externalId: data.imdbID,
                    url: 'http://www.imdb.com/title/' + data.imdbID
                };
                self.afterGetMovieMetaEvent.notify({
                    success: true,
                    movie: movie
                });
            } else {
                self.afterGetMovieMetaEvent.notify({
                    success: false,
                    movie: movie,
                    error: 'Invalid response from the server.',
                    serverResponse: data,
                    origParams: params,
                    options: options
                });
            }
        });
    }
};
