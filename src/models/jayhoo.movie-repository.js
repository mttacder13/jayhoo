jayhoo.MovieFileSystemRepository = function(fileSystem, dataPath) {

    // these 2 properties holds all the movies
    // _ids is for the original order of the movies
    // since _movies is an object and we can't
    // ensure the ordering of the movie objects in it
    // when we try to loop it
    this._ids = [];
    this._movies = {};

    this._fileSystem = fileSystem;
    this._dataPath = dataPath;
};

jayhoo.MovieFileSystemRepository.prototype = {

    /**
     * Adds the movie to the repository
     * @param {Object} movie The movie object
     */
    add: function(movie) {
        this.generateMovieId(movie);
        this.loadInfoFile(movie);

        this._ids.push(movie.id);
        this._movies[movie.id] = movie;
    },

    get: function(id) {
        return this._movies[id];
    },

    /**
     * Updates the file data of this movie
     * @param  {Object} movie The movie to be updated
     */
    update: function(movie) {
        this._fileSystem.writeTextFile(
            this._dataPath + movie.id + '.dat',
            JSON.stringify(movie.info)
            );
    },

    /**
     * Gets all the movies according to some filter and sorting options
     * @param  {Array}   filterOptions An array of filtering rules
     * @param  {Array}   sortOptions   An array of sorting rules
     * @param  {Boolean} strictFilter  Determines if the filtering rules should
     *                                 be strict (full-text comparison)
     * @return {Array}                 The array of movies filtered and sorted
     */
    getAll: function(filterOptions, sortOptions, strictFilter) {
        var movies;
        // if we have filter options, filter the movies
        if (filterOptions && filterOptions.length) {
            movies = this.getFilteredMovies(filterOptions, strictFilter);
        } else {
            // if nothing, then just return all movies
            movies = [];
            for (var i in this._ids) {
                movies.push(this._movies[this._ids[i]]);
            }
        }
        // sort array if we have sorting options
        if (sortOptions) {
            this.sortMovieArray(movies, sortOptions);
        }
        return movies;
    },

    /**
     * Each filterOption object has the following properties:
     *     field: the name of the field of the movie object to filter
     *     value: the expected value of the field of the movie object to filter
     *     exact: whether to check exact match (case-sensitive and whole value check)
     */
    getFilteredMovies: function(filterOptions, strictFilter) {
        var movies = [];

        // let's loop the ids since this is the proper ordering
        // when we added them in this repository
        // we cannot be ensured if we loop the hash object _movies
        for (var i in this._ids) {

            // first, let's get a referrence of this movie
            var movie = this._movies[this._ids[i]];

            // let's count if all filters do match (in case for strictFilter is true)
            var matchCount = 0;

            // let's loop through each filters
            for (var j in filterOptions) {
                // do compare exactly by case and by word
                var exact = filterOptions[j].exact;
                // what property of movie.info are we filtering against
                var field = filterOptions[j].field;
                // get the value that we will compare against
                // according whether we compare exactly or not
                var value = exact ? filterOptions[j].value : filterOptions[j].value.toLowerCase();
                // get the info property value
                var infoValue = exact ? movie.info[field] : movie.info[field].toLowerCase();
                if ((!exact && infoValue.indexOf(value) !== -1)
                    || (exact && infoValue == value)) {
                    matchCount++;
                    if (!strictFilter) {
                        break;
                    }
                }
            }
            if ((!strictFilter && matchCount)
                || (strictFilter && matchCount == filterOptions.length)) {
                movies.push(movie);
            }
        }
        return movies;
    },

    /**
     * Sort options
     *     type:  'file' or 'info' (default is 'info')
     *     field: the name of the field of the movie object
     *     order: 'asc' for ascending order (default is 'des')
     *            'des' for descending order
     */
    sortMovieArray: function(movies, sortOptions) {
        for (var i in sortOptions) {
            var sortOption = sortOptions[i];
            movies.sort(function(a, b) {
                var valueA, valueB;
                if (sortOption.type == 'file') {
                    valueA = a.file[sortOption.field];
                    valueB = b.file[sortOption.field];
                } else if (sortOption.type == 'info') {
                    valueA = a.info[sortOption.field];
                    valueB = b.info[sortOption.field];
                }
                if (valueA instanceof Date && valueB instanceof Date) {
                    valueA = (new Date(valueA)).getTime();
                    valueB = (new Date(valueB)).getTime();
                }
                if (valueA == undefined || isNaN(valueA)) return -1;
                if (valueB == undefined || isNaN(valueB)) return 1;
                if (valueA > valueB) return 1;
                if (valueB > valueA) return -1;
                return 0;
            });
            if (sortOption.order != 'asc') {
                movies.reverse();
            }
        }
    },

    /**
     * Create a quick hash of the path of the file that acts as an ID
     * We could have used the path but it's ugly to use it as an ID
     * Besides, it may not be a valid HTMLElement ID
     * @param  {Object} movie The movie
     */
    generateMovieId: function(movie) {

        var hash = movie.file.path.split('').reduce(function(a, b) {
            a = ((a << 5) - a) + b.charCodeAt(0);
            return a & a
        }, 0);

        movie.id = 'JAY' + hash + 'HOO';
    },

    /**
     * Read the data file of a movie and attach it to its info property
     * @param  {Object} movie the movie object
     */
    loadInfoFile: function(movie) {
        var infoJSON = this._fileSystem.readTextFile(this._dataPath + movie.id + '.dat');
        if (infoJSON) {
            try {
                movie.info = JSON.parse(infoJSON);
                movie.info.year = parseInt(movie.info.year);
                movie.info.released = new Date(movie.info.released);
                movie.info.rating = parseFloat(movie.info.rating);
            } catch (err) {
                this._fileSystem.deleteFile(movie.path);
            }
        }
    },

    /**
     * Delete data files that are no longer associated with movie files
     */
    deleteOrphanedDataFiles: function() {
        var self = this, count = 0;
        this._fileSystem.scanFolder(this._dataPath, function(fileInfo) {
            
            if (fileInfo.extension == 'dat' && !self._movies[fileInfo.name]) {
                self._fileSystem.deleteFile(fileInfo.path);
                count++;

                jayhoo.logger.info('Orphaned data file deleted:', fileInfo.path);
            }
        });
        return count;
    }
};
