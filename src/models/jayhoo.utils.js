/**
 * Simple utility for internationalization.
 * @param  {String} text The string to be translated.
 *                       This string can also contain placeholde (%s)
 *                       which will be replaced by corresponding
 *                       arguments passed here as well.
 * @return {String}      The translated text
 */
window._ = jayhoo.i18n = function(text) {
    text = jayhoo.i18n._lang[text] ? jayhoo.i18n._lang[text] : text;

    var argIndex = 1;
    while (text.indexOf('%s') != -1 && argIndex < arguments.length) {
        text = text.replace('%s', arguments[argIndex]);
        argIndex++;
    }

    return text;
}
jayhoo.i18n._lang = [];

/**
 * A simple utility for displaying dialog boxes.
 */
jayhoo.dialog = {
    alert: function(msg) {
        return window.alert(msg);
    },
    prompt: function(msg) {
        return window.prompt(msg, '');
    },
    confirm: function(msg) {
        return window.confirm(msg);
    }
}

/**
 * A simple utitity used to make JSONP (cross-domain) requests
 */
jayhoo.jsonp = function() {
    var requestCount = 0,
        head = document.querySelector('head');

    return {
        callbacks: {},
        get: function (url, data, callback) {
            if (typeof data == 'function') {
                callback = data;
                data = {};
            }
            url += (url.indexOf('?') + 1 ? '&' : '?');

            var script = document.createElement('script'),
                requestId = ++requestCount,
                self = this;

            data.callback = 'jayhoo.jsonp.callbacks.callback' + requestId;
            self.callbacks['callback' + requestId] = function (data) {
                head.removeChild(script);
                delete self.callbacks['callback' + requestId];
                callback(data);
            };

            var params = [];
            for (param in data) {
                params.push(param + '=' + encodeURIComponent(data[param]));
            }
            url += params.join('&');

            script.type = 'text/javascript';
            script.src = url;
            head.appendChild(script);
        }
    };
}();

/**
 * A simple file logger utility
 * @type {Object}
 */
jayhoo.logger = {
    _hasInitialized: false,
    init: function(path, fileSystem) {
        this._path = path + (new Date()).toISOString().replace(/[\-:]/ig, '.') + '.log';
        this._fileSystem = fileSystem;
        this._hasInitialized = true;
    },
    log: function(type, args) {
        if (this._hasInitialized) {
            if (!args) {
                args = [ type ];
                type = 'INFO ';
            }
            var arr = [];
            var date = new Date();
            var time = date.toISOString();
            time = time.substring(time.indexOf('T') + 1, time.length - 1);
            for (var i = 0, j = args.length; i < j; i++) {
                if (typeof args[i] == 'object') {
                    args[i] = JSON.stringify(args[i])
                }
                arr.push(args[i]);
            }
            this._fileSystem.writeTextFile(this._path, time + '    ' + type + '    ' + arr.join(', ') + "\r\n", true);
        }
    },
    info: function() {
        this.log('INFO ', arguments);
    },
    warn: function() {
        this.log('WARN ', arguments);
    },
    error: function() {
        this.log('ERROR', arguments);
    },
    debug: function() {
        this.log('DEBUG', arguments);
    }
};

function formatBytes(bytes, precision) {
    formatBytes.BYTE_STRINGS = formatBytes.BYTE_STRINGS || ['bytes', 'KB', 'MB', 'GB', 'TB'];
    precision = precision || 0;
    var suffix = 0;
    while (bytes >= 1024) {
        suffix++;
        bytes = bytes / 1024;
    }
    return parseFloat(bytes).toFixed(precision) + ' ' + formatBytes.BYTE_STRINGS[suffix];
}

function isValidUrl(string) {
    return /^http/.test(string);
}
