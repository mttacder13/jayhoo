/**
 * A simple object used to manage subscribing and publishing of events
 * @param {Object} sender The owner of this event
 */
jayhoo.Event = function(sender) {
    this._sender = sender;
    this._listeners = [];
}
jayhoo.Event.prototype = {
    /**
     * Attach an event listener
     * @param  {Function} callback (required) The function to be invoked when this event occurs
     * @param  {Object}   thisArg  (optional) The "this" context of the callback function when invoked
     *                                        Defaults to the owner of the event (_sender) when not provided
     */
    attach: function(callback, thisArg) {
        this._listeners.push({
            callback: callback,
            thisArg : thisArg || this._sender
        });
    },
    /**
     * This function is used by the event owner to publish an event
     * @param  {Object} args A hash object containing the arguments that will be passed to the listener when it is invoked
     * @return {Array}       Returns all values returned by all listeners' callbacks
     */
    notify: function(args) {
        var returnValues = [],
            returnValue,
            listener,
            i = 0, length = this._listeners.length;
        // default to an empty object if none is provided
        args = args || {};
        while (i < length) {
            listener = this._listeners[i];
            // call the listener's callback in the context of its thisArg property
            returnValue = listener.callback.call(listener.thisArg, args);
            returnValues.push(returnValue);
            i++;
        }
        return returnValues;
    }
};
