jayhoo.SearchBoxView = function() {
    this.searchEvent = new jayhoo.Event(this);

    this._containerElement = document.querySelector('.search-box');
    this._hintElement = this._containerElement.querySelector('.search-box-help');
    this._textboxElement = this._containerElement.querySelector('input[type="text"]');

    this.setStrings();

    var self = this;
    this._hintElement.addEventListener('click', function() {
        self._textboxElement.focus();
    });
    this._textboxElement.addEventListener('focus', function() {
        self._hintElement.style.display = 'none';
    });
    this._textboxElement.addEventListener('blur', function() {
        if (self._textboxElement.value.trim() == '') {
            self._textboxElement.value = '';
            self._hintElement.style.display = 'block';
        }
    });
    this._textboxElement.addEventListener('keyup', function(e) {
        if (e.keyCode == 13) {
            self.broadcastSearch();
        }
    });
    this._containerElement.querySelector('button').addEventListener('click', function() {
        self.broadcastSearch();
    });
}
jayhoo.SearchBoxView.prototype = {
    setStrings: function() {
        this._hintElement.innerHTML = _('Enter title (see help for advanced searching)');
    },
    broadcastSearch: function() {
        var returnValues = this.searchEvent.notify({
            value: this._textboxElement.value.trim()
        });
        this._textboxElement.value = returnValues[0];
    },
    clear: function() {
        this._textboxElement.value = '';
        this._hintElement.style.display = 'block';
    }
};
