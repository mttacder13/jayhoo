jayhoo.RatingView = function(containerElement, info) {
    containerElement.innerHTML = document.querySelector('#rating-template').innerHTML;
    if (info && info.rating && info.votes) {
        var valueElement = containerElement.querySelector('.star-value');
        valueElement.style.width = Math.floor(info.rating * 6.5) + 'px';
        containerElement.title = info.rating + ' (' + info.votes + ')';
    }
};
