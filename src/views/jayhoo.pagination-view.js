jayhoo.PaginationView = function() {

    this.pageClickedEvent = new jayhoo.Event(this);

    this._containerElement = document.querySelector('.pagination'),
    this._previousPageElement = this._containerElement.querySelector('.pagination-previous-page');
    this._nextPageElement = this._containerElement.querySelector('.pagination-next-page');

    this._currentPageElement = this._containerElement.querySelector('.pagination-current-page');
    this._lastPageElement = this._containerElement.querySelector('.pagination-last-page');
    this._totalItemsElement = this._containerElement.querySelector('.pagination-total-items');

    var self = this;
    this._containerElement.addEventListener('click', function(e) {
        e.preventDefault();
        switch (e.target.className) {
            case 'pagination-previous-page': self.pageClickedEvent.notify({ page: 'previous' }); break;
            case 'pagination-next-page': self.pageClickedEvent.notify({ page: 'next' }); break;
        }
    });
}
jayhoo.PaginationView.prototype = {
    update: function(pagination) {
        this._currentPageElement.innerHTML = pagination.currentPage;
        this._lastPageElement.innerHTML = pagination.totalPages;
        this._totalItemsElement.innerHTML = pagination.totalItems;
    }
};
