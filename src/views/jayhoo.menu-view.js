jayhoo.MenuView = function() {
    this.menuItemSelectedEvent = new jayhoo.Event(this);
    this._containerElement = document.querySelector('.menu');
    this._menuElements = this._containerElement.querySelectorAll('li');

    this.setStrings();

    var self = this;
    this._containerElement.addEventListener('click', function(e) {
        e.preventDefault();
        if (e.target.nodeName == 'A') {
            self.clearSelection();
            e.target.parentNode.className = 'selected';
            self.menuItemSelectedEvent.notify({
                menu: e.target.className
            });
        }
    });
};

jayhoo.MenuView.prototype = {
    selectDefault: function() {
        this._containerElement.querySelector('a').click();
    },
    clearSelection: function() {
        for (var i in this._menuElements) {
            this._menuElements[i].className = '';
        }
    },
    setStrings: function() {
        this._containerElement.querySelector('.menu-all').innerHTML = _('all movies');
        this._containerElement.querySelector('.menu-whats-new').innerHTML = _('what\'s new');
        this._containerElement.querySelector('.menu-safe-for-kids').innerHTML = _('safe for kids');
        this._containerElement.querySelector('.menu-latest-releases').innerHTML = _('latest release');
        this._containerElement.querySelector('.menu-top-rated').innerHTML = _('top rated');
    }
};
